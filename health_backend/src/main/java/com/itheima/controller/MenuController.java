package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;


import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.service.MenuService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
@RequestMapping("/menu")
public class MenuController {

    //dubbo
    @Reference
    private MenuService menuService;



    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult result = menuService.findpage(queryPageBean);
        return result;
    }



    //添加
    @RequestMapping("/add")
    public Result add(@RequestBody Menu menu) {
        try {
            menuService.add(menu);
            return new Result(true, MessageConstant.ADD_Menu_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_Menu_FAIL);
        }
    }
    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            menuService.delete(id);
            return new Result(true, MessageConstant.DELETE_Menu_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_Menu_FAIL);
        }
    }



    //按Id查询回显
    @RequestMapping("/findById")
    public Result findById( Integer id){
        try {
            Menu menu = menuService.findById(id);
            return new Result(true, MessageConstant.QUERY_Menu_SUCCESS,menu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_Menu_FAIL);
        }
    }


    //根据id查询菜单级别level
    @RequestMapping("/findLevelById")
    public Result findItemIdByGroupId(Integer id){
        try {
            Integer parentMenuId = menuService.findLevelById(id);
            System.out.println(parentMenuId);
            return new Result(true, MessageConstant.QUERY_Menu_SUCCESS,parentMenuId);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_Menu_FAIL);
        }
    }



    //修改
    @RequestMapping("/update")
    public Result update(@RequestBody Menu menu){
        try {
            menuService.update(menu);
            return new Result(true, MessageConstant.EDIT_Menu_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_Menu_FAIL);
        }
    }


    //根据level(父子标识)查询菜单
    @RequestMapping("/findAll")
    public Result findAll(Integer level){
        try {
            List<Menu> list = menuService.findAll(level);
            return new Result(true, MessageConstant.QUERY_Menu_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_Menu_FAIL);
        }
    }


    @RequestMapping("/getMenuList")
    public Result getMenuList() {

        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = user.getUsername();
            List<Menu> list = new ArrayList<>();
            Menu menu = new Menu();
            menu.setPath("1");
            menu.setName("工作台");
            menu.setIcon("fa-dashboard");
            list.add(menu);
            List<Menu> menuList = menuService.getMenuList(username);
            list.addAll(menuList);
            return new Result(true, MessageConstant.GET_MENU_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }
    }

}
