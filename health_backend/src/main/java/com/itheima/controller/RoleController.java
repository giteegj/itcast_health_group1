package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.itheima.entity.AddRoleBean;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.service.RoleManageService;
import com.qiniu.util.Json;
import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Reference
    private RoleManageService roleManageService;
    //新增弹出权限项
    @RequestMapping("/permissionFindAll")
    public Result permissionFindAll(){
        List<Permission> list= roleManageService.permissionFindAll();
        return new Result(true,"权限列表查询成功",list);
    }
    //新增弹出菜单项
    @RequestMapping("/menuFindAll")
    public Result menuFindAll(){
        List<Menu> list= roleManageService.menuFindAll();
        return new Result(true,"菜单列表查询成功",list);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBeen){
       PageResult pageResult=roleManageService.findPage(queryPageBeen);
        Long total = pageResult.getTotal();
        List rows = pageResult.getRows();
        return new PageResult(total,rows);
    }
    //添加
    @RequestMapping("/add")
    public Result add(@RequestBody AddRoleBean addRoleBean){

        try {
            roleManageService.add(addRoleBean);
            return new Result(true,"添加角色成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加角色失败");
        }
    }
    //基本信息回显
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Role role=roleManageService.findById(id);
            return new Result(true,"回显基本数据成功",role);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"回显基本数据失败");
        }
    }
    //权限信息回显
    @RequestMapping("/findPermissionIdsByRoleId")
    public Result findPermissionIdsByRoleId(Integer roleId){
        try {
            List<Integer> permissionIds=roleManageService.findPermissionIdsByRoleId(roleId);
            return new Result(true,"回显权限数据成功",permissionIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"回显权限数据失败");
        }
    }
    //菜单信息回显
    @RequestMapping("/findMenuIdsByRoleId")
    public Result findMenuIdsByRoleId(Integer roleId){
        try {
           List<Integer> menuIds=roleManageService.findMenuIdsByRoleId(roleId);
            return new Result(true,"回显菜单数据成功",menuIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"回显菜单数据失败");
        }
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody AddRoleBean addRoleBean){
        try {
            roleManageService.edit(addRoleBean);
            return new Result(true,"编辑角色成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"编辑角色失败");
        }
    }
    //删除
    @RequestMapping("/deleteByRoleId")
    public Result deleteByRoleId(Integer roleId){
        try {
            roleManageService.deleteByRoleId(roleId);
            return new Result(true,"删除角色成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除角色2失败");
        }
    }

}
