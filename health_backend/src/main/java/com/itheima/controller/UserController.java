package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Role;
import com.itheima.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Reference
    private UserService userService;

    //获取当前登录（认证）用户的用户名
    @RequestMapping("/getLoginUsername")
    public Result getLoginUsername(){
        try{
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = user.getUsername();
            String password = user.getPassword();
            Collection<GrantedAuthority> authorities = user.getAuthorities();
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,username);
        }catch (Exception e){
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){

        return userService.findPage(queryPageBean);
    }

    @RequestMapping("/add")
    //添加用户
    public Result add(@RequestBody com.itheima.pojo.User user,Integer[] roleIds){

        try {

            userService.add(user,roleIds);
            return new Result(true,"新增用户成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"新增用户失败");
        }
    }

    @RequestMapping("/delete")
    //删除用户
    public Result delete (Integer id){
        try {
            userService.delete(id);
            return new Result(true,"删除用户成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除用户失败");
        }

    }

    @RequestMapping("/findById")
    //根据id查询用户
    public Result findById(Integer id){
//        try {
        try {
            com.itheima.pojo.User user = userService.findById(id);
            return new Result(true,"查询用户成功",user);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"查询用户失败");
        }
    }
    //根据用户id查询到关联的角色id
    @RequestMapping("/findRoleIdsByUserId")
    public Result findRoleIdsByUserId(Integer id){
        try {
            List<Integer> list = userService.findRoleIdsByUserId(id);
            return new Result(true,"关联角色信息回显成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true,"关联角色信息回显失败");
        }
    }

    @RequestMapping("/edit")
    //修改用户
    public Result edit(@RequestBody com.itheima.pojo.User user,Integer[] roleIds){
        try {

            userService.edit(user,roleIds);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改失败");
        }
    }

    //回显角色
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Role> list= userService.findAll();
            return new Result(true,"查询角色成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"查询角色失败");
        }
    }
    //
}
