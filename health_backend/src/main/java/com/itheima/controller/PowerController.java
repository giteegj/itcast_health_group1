package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Permission;
import com.itheima.service.PowerService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

 //权限管理

@RestController
@RequestMapping("/power")
public class PowerController {

    @Reference
    private PowerService powerService;

     //分页查询权限
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
         PageResult pageResult=   powerService.findPage(queryPageBean);
                return  pageResult;
    }
    //添加权限
   @RequestMapping("/add")
    public  Result add(@RequestBody Permission permission){
       try {
           powerService.add(permission);
           return  new Result(true,MessageConstant.ADD_PERMISSION_SUCCESS);
       } catch (Exception e) {
           e.printStackTrace();
           return  new Result(false,MessageConstant.ADD_PERMISSION_FAIL);
       }
   }
   //编辑权限回显
   @RequestMapping("/findById")
   public Result findById(Integer id){
       try {
           Permission permission=  powerService.findById(id);
       return  new Result(true,MessageConstant.QUERY_PERMISSION_SUCCESS,permission);
       } catch (Exception e) {
           e.printStackTrace();
           return  new Result(false,MessageConstant.QUERY_PERMISSION_FAIL);
       }
   }
   // 编辑权限保存
  @RequestMapping("/edit")
    public  Result edit(@RequestBody Permission permission){
      try {
          powerService.edit(permission);
          return new Result(true,MessageConstant.EDIT_PERMISSION_SUCCESS);
      } catch (Exception e) {
          e.printStackTrace();
          return  new Result(false,MessageConstant.EDIT_PERMISSION_FAIL);
      }
  }
 //删除权限  根据id
 @RequestMapping("/delete")
 public Result delete(Integer id){
     try {
         powerService.delete(id);
      return  new Result(true,MessageConstant.DELETE_PERMISSION_SUCCESS);
     } catch (Exception e) {
         e.printStackTrace();
         return  new Result(false,MessageConstant.DELETE_PERMISSION_FAIL);
     }
 }


}
