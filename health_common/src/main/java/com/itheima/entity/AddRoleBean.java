package com.itheima.entity;

import com.itheima.pojo.Role;

import java.io.Serializable;

public class AddRoleBean implements Serializable {
    private Integer[] permissionIds;
    private Integer[] menuIds;
    private Role formData;

    public Integer[] getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(Integer[] permissionIds) {
        this.permissionIds = permissionIds;
    }

    public Integer[] getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(Integer[] menuIds) {
        this.menuIds = menuIds;
    }

    public Role getFormData() {
        return formData;
    }

    public void setFormData(Role formData) {
        this.formData = formData;
    }
}
