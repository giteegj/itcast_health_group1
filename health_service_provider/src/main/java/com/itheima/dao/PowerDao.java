package com.itheima.dao;

import com.itheima.pojo.Permission;

import java.util.List;

public interface PowerDao {
    //分页查询权限
   public  List<Permission> findPage(String queryString);
  //添加权限
   public void add(Permission permission);
   //编辑权限回显
   public  Permission findById(Integer id);
   //编辑权限保存
   public void edit(Permission permission);
   //删除权限前  判断和角色表是否有 关联数据
    public long selectRoleCount(Integer permissionid);
    //删除权限
    public void delete(Integer id);
}
