package com.itheima.dao;

import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleManageDao {
    public List<Permission> permissionFindAll();

    public List<Menu> menuFindAll();

    List<Role> findPage(String queryString);

    void add(Role role);

    void setRolePermission(Map map);

    void setRoleMenu(Map map);

    Role findById(Integer id);

    List<Integer> findPermissionIdsByRoleId(Integer id);

    List<Integer> findMenuIdsByRoleId(Integer id);

    void edit(Role formData);

    void deleteRolePermission(Integer id);

    void deleteRoleMenu(Integer id);

    void deleteByRoleId(Integer id);
}
