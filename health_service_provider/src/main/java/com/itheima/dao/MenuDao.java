package com.itheima.dao;

import com.itheima.pojo.Menu;
import org.apache.ibatis.annotations.Param;
import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Menu;

import java.util.List;
import java.util.Map;
import java.util.List;

public interface MenuDao {
    public List<Menu> findPage(String queryString);

    List<Menu> findAll(Integer level);

    Integer findLevelById(Integer id);

    void add( Menu menu);

    Menu findById(Integer id);

    void update(Menu menu);

    void delete(Integer id);

    Integer findSonCountById(Integer id);

    public List<Menu> getMenuList(String username);
}
