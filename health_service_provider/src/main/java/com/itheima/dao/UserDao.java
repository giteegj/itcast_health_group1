package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserDao {
    public User findByUsername(String username);

    //分页查询
    public Page<User> findByCondition(String queryString);
    //添加用户
    public void add(User user);
    //根据id删除用户
    public void delete(Integer id);
    //根据id查询用户
    public User findById(Integer id);
    //修改用户
    public void edit(User user);
    //根据用户id查询是否关联角色
    public long findCountByRole(Integer id);
    //遍历查询角色
    public List<Role> findAll();
    //<!--添加用户和角色的关联关系-->
    public void SetUserAndRole(Map map);
    //根据用户id查询到关联的角色id
    public List<Integer> findRoleIdsByUserId(Integer id);
    //清除roleId和Userid的关系
    public void deleteRoleIdByUserId(Integer id);
}
