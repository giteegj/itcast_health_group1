package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户服务
 */
@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;
    //根据用户名查询用户信息,包括用户的角色和角色关联的权限
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);//根据用户名查询用户表
        if(user == null){
            return null;
        }
        Integer userId = user.getId();
        //根据用户id查询关联的角色
        Set<Role> roles = roleDao.findByUserId(userId);
        if(roles != null && roles.size() > 0){
            //遍历角色集合，查询每个角色关联的权限
            for (Role role : roles) {
                Integer roleId = role.getId();//角色id
                //根据角色id查询关联的权限
                Set<Permission> permissions = permissionDao.findByRoleId(roleId);
                if(permissions != null && permissions.size() > 0){
                    //角色关联权限集合
                    role.setPermissions(permissions);
                }
            }
            //用户关联角色集合
            user.setRoles(roles);
        }

        return user;
    }

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
            //当前页码，每页查询条数
        //自动的对PageHelper.startPage 方法下的第一个sql 查询进行分页
       PageHelper.startPage(currentPage, pageSize);
        Page<User> userPage = userDao.findByCondition(queryString);

        return new PageResult(userPage.getTotal(),userPage.getResult());
    }

    //添加用户
    @Override
    //新加用户时,需要让用户和其对应角色进行关联
    public void add(User user,Integer[] roleIds) {
        //新增用户.操作t_user
        userDao.add(user);
         //设置用户和角色的多对多的关联关系，操作t_user_role表
        //获取用户id 即为主键id
        Integer id = user.getId();
        if(roleIds != null && roleIds.length > 0) {
            for (Integer roleId : roleIds) {
                Map<String, Integer> map = new HashMap<>();
                map.put("userId", id);
                map.put("roleId", roleId);
                userDao.SetUserAndRole(map);
            }
        }
    }

    //根据id删除用户
    @Override
    public void delete(Integer id) {
        //判断用户是否关联角色
        long countByRole = userDao.findCountByRole(id);
        if (countByRole>0){
            //该用户关联角色.不能删除
            throw new RuntimeException();
        }
        userDao.delete(id);
    }

    //根据id查询用户 回显
    @Override
    public User findById(Integer id) {
        return   userDao.findById(id);
    }

    //根据用户id查询到关联的角色id
    @Override
    public List<Integer> findRoleIdsByUserId(Integer id) {
        return userDao.findRoleIdsByUserId(id);
    }

    //修改用户
    @Override
    public void edit(User user,Integer[] roleIds) {
        userDao.edit(user);

      userDao.deleteRoleIdByUserId(user.getId());

        Integer id = user.getId();
        if(roleIds != null && roleIds.length > 0) {
            for (Integer roleId : roleIds) {
                Map<String, Integer> map = new HashMap<>();
                map.put("userId", id);
                map.put("roleId", roleId);
                userDao.SetUserAndRole(map);
            }
        }
    }
    //查询角色集合
    @Override
    public List<Role> findAll() {

        return userDao.findAll();
    }

}
