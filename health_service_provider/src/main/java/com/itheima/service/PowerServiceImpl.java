package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.container.page.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.PowerDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = PowerService.class)
@Transactional
public class PowerServiceImpl implements PowerService{
    @Autowired
    private PowerDao powerDao;
       //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {

        Integer pageSize = queryPageBean.getPageSize();
        Integer currentPage = queryPageBean.getCurrentPage();
        String queryString = queryPageBean.getQueryString();

        PageHelper.startPage(currentPage,pageSize);
        List<Permission> resultList= powerDao.findPage(queryString);
           PageInfo  pageInfo =new PageInfo(resultList);
        List rows = pageInfo.getList(); //结果集
        long total = pageInfo.getTotal(); //总记录数

        return  new PageResult(total,rows);
    }
     //添加权限
    @Override
    public void add(Permission permission) {
        powerDao.add(permission);
    }
    //编辑权限回显
    @Override
    public Permission findById(Integer id) {
       Permission permission= powerDao.findById(id);
              return permission;
    }
    //编辑权限保存
    @Override
    public void edit(Permission permission) {
         powerDao.edit(permission);
    }
   // 删除权限 根据 id
    @Override
    public void delete(Integer id) {
        //删除权限前  判断和角色表是否有 关联数据
        long count = powerDao.selectRoleCount(id);
        if(count>0){ // 大于0 说明有关联数据
          throw new RuntimeException(MessageConstant.DELETE_PERMISSION_FAIL);
        }else {
            //可以删除
            powerDao.delete(id);
        }

    }


}
