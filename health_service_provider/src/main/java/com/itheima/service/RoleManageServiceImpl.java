package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.RoleManageDao;
import com.itheima.entity.AddRoleBean;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RoleManageService.class)
@Transactional
public class RoleManageServiceImpl implements RoleManageService {
    @Autowired
    private RoleManageDao roleManageDao;
    @Override
    public List<Permission> permissionFindAll() {
        List<Permission> list= roleManageDao.permissionFindAll();
        return list;
    }

    @Override
    public List<Menu> menuFindAll() {
        List<Menu> list= roleManageDao.menuFindAll();
        return list;
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBeen) {
        Integer currentPage = queryPageBeen.getCurrentPage();
        Integer pageSize = queryPageBeen.getPageSize();
        String queryString = queryPageBeen.getQueryString();
        PageHelper.startPage(currentPage,pageSize);
        List<Role> list=roleManageDao.findPage(queryString);
        PageInfo pageInfo=new PageInfo(list);
        long total = pageInfo.getTotal();
        List list1 = pageInfo.getList();
        return new PageResult(total,list1);
    }

    @Override
    public void add(AddRoleBean addRoleBean) {
        //添加基本信息
        Role role = addRoleBean.getFormData();
        Integer[] menuIds = addRoleBean.getMenuIds();
        Integer[] permissionIds = addRoleBean.getPermissionIds();
        roleManageDao.add(role);
        Integer id = role.getId();
        //设置中间表t_role_permission关联关系
        this.setRolePermission(id,permissionIds);
        //设置中间表t_role_menu关联关系
        this.setRoleMenu(id,menuIds);
    }

    @Override
    public List<Integer> findPermissionIdsByRoleId(Integer id) {
        List<Integer> permissionIds=roleManageDao.findPermissionIdsByRoleId(id);
        return permissionIds;
    }

    @Override
    public List<Integer> findMenuIdsByRoleId(Integer id) {
        List<Integer> menuIds=roleManageDao.findMenuIdsByRoleId(id);
        return menuIds;
    }

    @Override
    public Role findById(Integer id) {
        Role role=roleManageDao.findById(id);
        return role;
    }

    @Override
    public void edit(AddRoleBean addRoleBean) {
        Role formData = addRoleBean.getFormData();
        Integer id = formData.getId();
        Integer[] permissionIds = addRoleBean.getPermissionIds();
        Integer[] menuIds = addRoleBean.getMenuIds();
        //修改基本信息
        roleManageDao.edit(formData);
        //删除中间键
        roleManageDao.deleteRolePermission(id);
        roleManageDao.deleteRoleMenu(id);
        //重新建立中间键关系
        this.setRolePermission(id,permissionIds);
        this.setRoleMenu(id,menuIds);

    }

    @Override
    public void deleteByRoleId(Integer id) {
        //删除RoleMenu中间键
        List<Integer> permissionIdsByRoleId = roleManageDao.findPermissionIdsByRoleId(id);
        List<Integer> menuIdsByRoleId = roleManageDao.findMenuIdsByRoleId(id);
        if (permissionIdsByRoleId.size()>0 && id!=null){
            //删除RolePermission中间键
            roleManageDao.deleteRolePermission(id);
        }
        if(menuIdsByRoleId.size()>0 && id!=null){
            //删除RoleMenu中间键
            roleManageDao.deleteRoleMenu(id);
        }
        //删除基本信息
        roleManageDao.deleteByRoleId(id);
    }

    private void setRoleMenu(Integer id, Integer[] menuIds) {
        if (id !=null && menuIds.length>0){
            for (Integer menuId : menuIds) {
                Map map=new HashMap();
                map.put("id",id);
                map.put("menuId",menuId);
                roleManageDao.setRoleMenu(map);
            }
        }

    }

    private void setRolePermission(Integer id,Integer[] permissionIds) {
        //通过id查出permissionIds
        if(permissionIds.length>0 && id !=null){
            for (Integer permissionId : permissionIds) {
                Map map=new HashMap();
                map.put("id",id);
                map.put("permissionId",permissionId);
                roleManageDao.setRolePermission(map);
            }
        }

    }
}
