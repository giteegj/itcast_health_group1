package com.itheima.service;


import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;

import java.util.List;

public interface UserService {
    public User findByUsername(String username);

    //分页查询
    public PageResult findPage(QueryPageBean queryPageBean);
    //添加用户
    public void add(User user ,Integer[] roleIds);
    //根据id删除用户
    public void delete(Integer id);
    //根据id回显用户
    public User findById(Integer id);
    //修改用户
    public void edit(User user,Integer[] roleIds);
    //遍历查询角色
    public List<Role> findAll();
    //根据用户id查询到关联的角色id
    public List<Integer> findRoleIdsByUserId(Integer id);

}
