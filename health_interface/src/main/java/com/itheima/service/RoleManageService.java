package com.itheima.service;

import com.itheima.entity.AddRoleBean;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;

import java.util.List;

public interface RoleManageService {
    List<Permission> permissionFindAll();

    List<Menu> menuFindAll();

    PageResult findPage(QueryPageBean queryPageBeen);

    void add(AddRoleBean addRoleBean);

    List<Integer> findPermissionIdsByRoleId(Integer id);

    List<Integer> findMenuIdsByRoleId(Integer id);

    Role findById(Integer id);

    void edit(AddRoleBean addRoleBean);

    void deleteByRoleId(Integer roleId);
}
