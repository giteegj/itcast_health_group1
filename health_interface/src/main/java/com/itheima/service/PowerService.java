package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;

public interface PowerService {
    //分页查询
    public PageResult findPage(QueryPageBean queryPageBean);
     //添加权限
     public void add(Permission permission);
    // 编辑权限回显
    public Permission findById(Integer id);
    //编辑权限保存
    public void edit(Permission permission);
    // 删除权限 根据id
     public void delete(Integer id);
}
