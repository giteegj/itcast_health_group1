package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;

import java.util.List;

public interface MenuService {
    public List<Menu> getMenuList(String username);
    PageResult findpage(QueryPageBean queryPageBean);
    List<Menu> findAll(Integer level);
    Integer findLevelById(Integer id);
    void add(Menu menu);
    Menu findById(Integer id);
    void update(Menu menu);
    void delete(Integer id);
}
