package com.itheima.service;

import com.itheima.pojo.Member;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MemberService {
    public Member findByTelephone(String telephone);
    public void add(Member member);
    public List<Integer> findMemberCountByMonths(List<String> months);

    public List<Map> findMemberReportBySex();

    public List<Map> getMemberReportByAge();


    // 根据开始时间和结束时间条件获取会员统计数量
    public Map<String, List> findMemberCountByCondition(Date startTime, Date endTime);
}
